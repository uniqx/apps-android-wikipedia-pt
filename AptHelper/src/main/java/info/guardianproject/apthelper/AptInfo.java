package info.guardianproject.apthelper;

import java.net.InetSocketAddress;

public class AptInfo {

    private InetSocketAddress httpProxyConfig;

    public void setHttpProxyConfig(InetSocketAddress httpProxyConfig) {
        this.httpProxyConfig = httpProxyConfig;
    }

    public InetSocketAddress getHttpProxyConfig() {
        return httpProxyConfig;
    }
}
