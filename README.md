### Wikipedia Android PT

This a prototype version of Wikipeda Android enabling the App to use pluggable transports.

Builds are available as F-Droid Repsoitory: https://gitlab.com/uniqx/apps-android-wikipedia-pt-nightly/raw/master/fdroid/repo?fingerprint=4E9C0CD01B2BE68F88960F72F019BE0E70E754F80176C066DC5189BBCBCD436D  

(Currently only tested for Android Emulator x86 API 28; for manual download see: https://gitlab.com/uniqx/apps-android-wikipedia-pt-nightly/tree/master/fdroid/repo)