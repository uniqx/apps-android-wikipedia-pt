package org.wikipedia.net;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.AtomicFile;
import android.util.Log;
import android.webkit.WebView;

import org.wikipedia.WikipediaApp;
import org.wikipedia.dataclient.okhttp.OkHttpConnectionFactory;
import org.wikipedia.util.log.L;

import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.concurrent.atomic.AtomicReference;

import info.guardianproject.netcipher.webkit.WebkitProxy;
import okhttp3.OkHttpClient;

public class ProxyHelper {

    private final Context appContext;
    private OkHttpClient okHttpClient;

    private static String proxyHost = "";
    private static int proxyPort = -1;


    public ProxyHelper(Context context) {
        appContext = context.getApplicationContext();
    }

    public static void setHttpProxy(Context context, String proxyHost, int proxyPort) {
        ProxyHelper.proxyHost = proxyHost;
        ProxyHelper.proxyPort = proxyPort;

        configureWebViev(context);
        OkHttpConnectionFactory.invalidateClient();
    }

    public static void configureWebViev(Context context) {
        // TODO: maybe ip+port should be configurable
        try {
            WebkitProxy.setProxy(WikipediaApp.class.getName(), context, null, proxyHost, proxyPort);
        } catch (Exception e) {
            Log.e("#pt", "could not set webkit proxy", e);
        }
    }

    public OkHttpClient.Builder okHttp(OkHttpClient.Builder okHttpClientBuilder){
        if (proxyHost != null && proxyPort > 0) {
            return okHttpClientBuilder.proxy(nonAsyncNewProxyInstanceHack(proxyHost, proxyPort));
        } else {
            return okHttpClientBuilder;
        }
    }

    /**
     * new Proxy() ... raises Exception because it's not allowed on main-thread.
     */
    private Proxy nonAsyncNewProxyInstanceHack(final String proxyHost, final int proxyPort){
        HandlerThread t = new HandlerThread("nonAsyncHackThread");
        t.start();
        Handler handler = new Handler(t.getLooper());

        AtomicReference<Proxy> p = new AtomicReference<>();
        handler.post(new Runnable() {
            @Override
            public void run() {
                p.set(new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyHost, proxyPort)));
            }
        });

        t.quitSafely();
        try {
            t.getLooper().getThread().join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return p.get();
    }
}
